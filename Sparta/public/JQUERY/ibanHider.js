$(document).ready(function () {
    $('.rolesHide').change(function (event) {
        if (event['currentTarget']['value'] === 'ROLE_PAYED') {
            $('.iban').show();
            $(".req").prop('required', true);
        } else {
            $('.iban').hide();
            $(".req").prop('required', false);
        }
        }

    );
});