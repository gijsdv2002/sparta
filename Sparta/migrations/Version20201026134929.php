<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201026134929 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE IF NOT EXISTS bank  (id INT AUTO_INCREMENT NOT NULL, iban VARCHAR(255) NOT NULL, card_holder VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS document (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, img VARCHAR(255) DEFAULT NULL, document VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS forum_category (id INT AUTO_INCREMENT NOT NULL, subject_id_id INT NOT NULL, user_id_id INT NOT NULL, title VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, reactions INT NOT NULL, INDEX IDX_21BF94266ED75F8F (subject_id_id), INDEX IDX_21BF94269D86650F (user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS forum_subject (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS forum_text (id INT AUTO_INCREMENT NOT NULL, category_id_id INT NOT NULL, user_id_id INT NOT NULL, text VARCHAR(255) NOT NULL, img VARCHAR(255) DEFAULT NULL, INDEX IDX_F86A07109777D11E (category_id_id), INDEX IDX_F86A07109D86650F (user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS model (id INT AUTO_INCREMENT NOT NULL, img VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, text VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS model_info (id INT AUTO_INCREMENT NOT NULL, model_id_id INT NOT NULL, type VARCHAR(255) NOT NULL, info VARCHAR(255) NOT NULL, INDEX IDX_E6DE1DB04107BEA0 (model_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS news (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS personal_detail (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) DEFAULT NULL, middlename VARCHAR(255) DEFAULT NULL, lastname VARCHAR(255) DEFAULT NULL, birthday DATE DEFAULT NULL, gender VARCHAR(20) DEFAULT NULL, streetname_housenumber VARCHAR(255) DEFAULT NULL, zipcode VARCHAR(20) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, province VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, phonenumber VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS slider (id INT AUTO_INCREMENT NOT NULL, model_id_id INT NOT NULL, slider_img VARCHAR(255) NOT NULL, INDEX IDX_CFC710074107BEA0 (model_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS user (id INT AUTO_INCREMENT NOT NULL, personal_detail_id_id INT DEFAULT NULL, bank_id_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, profile_image VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D649E4019F7C (personal_detail_id_id), INDEX IDX_8D93D64975EE5022 (bank_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE forum_category ADD CONSTRAINT FK_21BF94266ED75F8F FOREIGN KEY (subject_id_id) REFERENCES forum_subject (id)');
        $this->addSql('ALTER TABLE forum_category ADD CONSTRAINT FK_21BF94269D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE forum_text ADD CONSTRAINT FK_F86A07109777D11E FOREIGN KEY (category_id_id) REFERENCES forum_category (id)');
        $this->addSql('ALTER TABLE forum_text ADD CONSTRAINT FK_F86A07109D86650F FOREIGN KEY (user_id_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE model_info ADD CONSTRAINT FK_E6DE1DB04107BEA0 FOREIGN KEY (model_id_id) REFERENCES model (id)');
        $this->addSql('ALTER TABLE slider ADD CONSTRAINT FK_CFC710074107BEA0 FOREIGN KEY (model_id_id) REFERENCES model (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649E4019F7C FOREIGN KEY (personal_detail_id_id) REFERENCES personal_detail (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64975EE5022 FOREIGN KEY (bank_id_id) REFERENCES bank (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64975EE5022');
        $this->addSql('ALTER TABLE forum_text DROP FOREIGN KEY FK_F86A07109777D11E');
        $this->addSql('ALTER TABLE forum_category DROP FOREIGN KEY FK_21BF94266ED75F8F');
        $this->addSql('ALTER TABLE model_info DROP FOREIGN KEY FK_E6DE1DB04107BEA0');
        $this->addSql('ALTER TABLE slider DROP FOREIGN KEY FK_CFC710074107BEA0');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649E4019F7C');
        $this->addSql('ALTER TABLE forum_category DROP FOREIGN KEY FK_21BF94269D86650F');
        $this->addSql('ALTER TABLE forum_text DROP FOREIGN KEY FK_F86A07109D86650F');
        $this->addSql('DROP TABLE bank');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE forum_category');
        $this->addSql('DROP TABLE forum_subject');
        $this->addSql('DROP TABLE forum_text');
        $this->addSql('DROP TABLE model');
        $this->addSql('DROP TABLE model_info');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE personal_detail');
        $this->addSql('DROP TABLE slider');
        $this->addSql('DROP TABLE user');
    }
}
