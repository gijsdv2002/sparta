<?php

namespace App\Entity;

use App\Repository\ModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ModelRepository::class)
 */
class Model
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $img;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $text;

    /**
     * @ORM\OneToMany(targetEntity=ModelInfo::class, mappedBy="modelID",cascade={"persist"})
     */
    private $modelInfoID;

    /**
     * @ORM\OneToMany(targetEntity=Slider::class, mappedBy="modelID",cascade={"persist"})
     */
    private $sliderID;

    /**
     * @ORM\Column(type="json")
     */
    private $category = [];

    public function __construct()
    {
        $this->modelInfoID = new ArrayCollection();
        $this->sliderID = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Collection|ModelInfo[]
     */
    public function getModelInfoID(): Collection
    {
        return $this->modelInfoID;
    }

    public function addModelInfoID(ModelInfo $modelInfoID): self
    {
        if (!$this->modelInfoID->contains($modelInfoID)) {
            $this->modelInfoID[] = $modelInfoID;
            $modelInfoID->setModelID($this);
        }

        return $this;
    }

    public function removeModelInfoID(ModelInfo $modelInfoID): self
    {
        if ($this->modelInfoID->contains($modelInfoID)) {
            $this->modelInfoID->removeElement($modelInfoID);
            // set the owning side to null (unless already changed)
            if ($modelInfoID->getModelID() === $this) {
                $modelInfoID->setModelID(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Slider[]
     */
    public function getSliderID(): Collection
    {
        return $this->sliderID;
    }

    public function addSliderID(Slider $sliderID): self
    {
        if (!$this->sliderID->contains($sliderID)) {
            $this->sliderID[] = $sliderID;
            $sliderID->setModelID($this);
        }

        return $this;
    }

    public function removeSliderID(Slider $sliderID): self
    {
        if ($this->sliderID->contains($sliderID)) {
            $this->sliderID->removeElement($sliderID);
            // set the owning side to null (unless already changed)
            if ($sliderID->getModelID() === $this) {
                $sliderID->setModelID(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?array
    {
        return $this->category;
    }

    public function setCategory(array $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
