<?php

namespace App\Entity;

use App\Repository\SliderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SliderRepository::class)
 */
class Slider
{
    protected $file;


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Model::class, inversedBy="sliderID")
     * @ORM\JoinColumn(nullable=false)
     */
    private $modelID;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slider_img;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModelID(): ?Model
    {
        return $this->modelID;
    }

    public function setModelID(?Model $modelID): self
    {
        $this->modelID = $modelID;

        return $this;
    }

    public function getSliderImg(): ?string
    {
        return $this->slider_img;
    }

    public function setSliderImg(string $slider_img): self
    {
        $this->slider_img = $slider_img;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = $file;
    }
}
