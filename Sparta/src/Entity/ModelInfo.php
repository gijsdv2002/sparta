<?php

namespace App\Entity;

use App\Repository\ModelInfoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ModelInfoRepository::class)
 */
class ModelInfo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Model::class, inversedBy="modelInfoID")
     * @ORM\JoinColumn(nullable=false)
     */
    private $modelID;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $info;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModelID(): ?Model
    {
        return $this->modelID;
    }

    public function setModelID(?Model $modelID): self
    {
        $this->modelID = $modelID;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(string $info): self
    {
        $this->info = $info;

        return $this;
    }
}
