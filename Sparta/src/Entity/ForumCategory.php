<?php

namespace App\Entity;

use App\Repository\ForumCategoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ForumCategoryRepository::class)
 */
class ForumCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ForumSubject::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $subjectID;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $userID;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     */
    private $reactions;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubjectID(): ?ForumSubject
    {
        return $this->subjectID;
    }

    public function setSubjectID(?ForumSubject $subjectID): self
    {
        $this->subjectID = $subjectID;

        return $this;
    }

    public function getUserID(): ?user
    {
        return $this->userID;
    }

    public function setUserID(?user $userID): self
    {
        $this->userID = $userID;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getReactions(): ?int
    {
        return $this->reactions;
    }

    public function setReactions(int $reactions): self
    {
        $this->reactions = $reactions;

        return $this;
    }
}
