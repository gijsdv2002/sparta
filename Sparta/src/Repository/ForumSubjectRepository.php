<?php

namespace App\Repository;

use App\Entity\ForumSubject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ForumSubject|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForumSubject|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForumSubject[]    findAll()
 * @method ForumSubject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForumSubjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForumSubject::class);
    }

    public function findAllGroupedByCategory()
    {
        return $this->createQueryBuilder('f')
            ->orderBy('f.category', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

}
