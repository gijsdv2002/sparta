<?php

namespace App\Repository;

use App\Entity\ForumText;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ForumText|null find($id, $lockMode = null, $lockVersion = null)
 * @method ForumText|null findOneBy(array $criteria, array $orderBy = null)
 * @method ForumText[]    findAll()
 * @method ForumText[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ForumTextRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ForumText::class);
    }
}

