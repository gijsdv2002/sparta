<?php

namespace App\Repository;

use App\Entity\ModelInfo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ModelInfo|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModelInfo|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModelInfo[]    findAll()
 * @method ModelInfo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModelInfoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ModelInfo::class);
    }

    /**
    * @return ModelInfo[] Returns an array of ModelInfo objects
    */
    public function findInfo($id)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.modelID = :id')
            ->setParameter('id', $id)
            ->OrderBy('m.type','')
            ->addOrderBy('m.type', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
}
