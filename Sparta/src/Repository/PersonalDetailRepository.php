<?php

namespace App\Repository;

use App\Entity\PersonalDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PersonalDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonalDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonalDetail[]    findAll()
 * @method PersonalDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonalDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonalDetail::class);
    }

    // /**
    //  * @return PersonalDetail[] Returns an array of PersonalDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PersonalDetail
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
