<?php

namespace App\Form;

use App\Entity\Model;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ModelFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('img', FileType::class, [
                'label' => 'Foto van model toevoegen: ',
                'label_attr' => ['class'=> 'mtpl-fls'],
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'mb-4',
                ],
                'required' => true,
                'mapped' => 'false',
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png'
                        ]
                        ,
                        'mimeTypesMessage' => 'Upload alstublieft ene geldig foto type(png, jpeg , jpg)',

                    ])
                ]
            ])
            ->add('name', TextType::class,[
                'label'=>'naam: ',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'w-100 mh-25 flush',
                    'placeholder' => 'naam'
                ]])
            ->add('text', TextareaType::class,[
                'label'=>'beschrijving toevoegen: ',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'w-100 mh-25 flush',
                    'placeholder' => 'tekst',
                    ]])
//            includes mutiple modelInfo forms detail form in form
            ->add('modelInfoID', CollectionType::class, [
                'label' => false,
                'error_bubbling' => true,
                'entry_type'=> ModelInfoFormType::class,
                'entry_options'=>['label'=>false],
                'allow_add' => true,
                'block_name' => 'model_info',
                'by_reference'=>false
            ])
//            includes mutiple slider forms detail form in form
            ->add('sliderID', CollectionType::class, [
                'label' => false,
                'error_bubbling' => true,
                'entry_type'=> SliderFormType::class,
                'entry_options'=>['label'=>false],
                'allow_add' => true,
                'block_name' => 'slider',
                'by_reference'=>false
            ])
            ->add('category', ChoiceType::class,[
                'label'=>'categorie kiezen: ',
                'error_bubbling' => true,
                'required'=>true,
                'choices'=>[
                    'Nozembromfiets '=> "Nozembromfiets",
                    'Herenbromfiets '=> "Herenbromfiets",
                    'Damesbromfiets '=> "Damesbromfiets",
                    'Transport Bromfiets'=> "Transport Bromfiets",
                    'Verpleegsters Bromfiets'=> "Verpleegsters Bromfiets",
                     'Exotische Type\'s '=> "Exotische Type\'s",
                ],
                'attr' => ['class' => ''],
                'expanded'=>true,
                'multiple'=>true
            ] )
            ->add('submit', SubmitType::class, [
                'label'=>'versturen',
                'attr' => [
                    'class' => 'btn-light cst-btn bg-color text-white mt-3 form-control cst-sub mb-5'
                ]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Model::class,
        ]);
    }
}
