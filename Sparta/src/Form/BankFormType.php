<?php

namespace App\Form;

use App\Entity\Bank;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BankFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('IBAN', TextType::class,[
                'label'=>'IBANnummer',
                'error_bubbling' => true,
                'attr' => ['class'=> 'w-100 req'],
                'required'=> false
            ])
            ->add('card_holder', TextType::class,[
                'label'=>'Rekeninghouder',
                'error_bubbling' => true,
                'attr' => ['class'=> 'w-100 req'],
                'required'=> false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Bank::class,
        ]);
    }
}
