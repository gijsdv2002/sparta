<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label'=> 'Emailadres*',
                'required' => true,
                'error_bubbling' => true,
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'error_bubbling' => true,
                'invalid_message' => 'Wachtwoord veld moet het zelfde zijn',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options' => ['label' => 'Wachtwoord*'],
                'second_options' => ['label' => 'Wachtwoord herhalen*'],
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'voer wachtwoord in',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Uw wachtwoord moet maximaal {{ limit }} karakters bevatten',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('profilename', TextType::class, [
                'label' => 'Gebruikersnaam*',
                'error_bubbling' => true,
                'required' => true
            ])
            ->add('profileImage', FileType::class, [
                'label' => 'Profielfoto',
                'required' => false,
                'mapped' => false,
                'error_bubbling' => true,
                'constraints' => new File(['maxSize' => '2048k', 'mimeTypes' => ['image/jpeg', 'image/png',], 'mimeTypesMessage' => 'Upload alstublieft ene geldig foto type(png, jpeg , jpg)'])])
//            includes Personal detail form in form
            ->add('personalDetailID', PersonalDetailFormType::class)
            ->add('roles', ChoiceType::class, [
                'label' => 'Status',
                'error_bubbling' => true,
                'choices' => [
                    'Lid' => "ROLE_USER",
                    'Betaald lid' => "ROLE_PAYED"
                ],
                'attr' => ['class' => 'rolesHide'],
                'required' => true,
            ])
//            includes Bank form in form
            ->add('bankID', BankFormType::class);

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(function ($rolesArray) {
                return count($rolesArray) ? $rolesArray[0] : null;
            },
                function ($rolesString) {
                    return [$rolesString];
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
