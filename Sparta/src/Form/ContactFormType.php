<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'naam', 'required' => true,
                'error_bubbling' => true,
                'attr' => array(
                    'class' => 'w-100 mh-25',
                    'placeholder' => 'naam'
                )])
            ->add('bio', TextareaType::class, [
                'label' => 'biografie',
                'required' => true,
                'error_bubbling' => true,
                'attr' => array(
                    'class' => 'w-100 mh-25 flush',
                    'placeholder' => 'biografie',
                    'style' => 'margin-bottom: -0.4em'
                )])
            ->add('email', EmailType::class, [
                'label' => 'email', 'required' => true, 'attr' => array(
                    'class' => 'w-100 mh-25 ',
                    'placeholder' => 'email',
                )])
            ->add('img', FileType::class, [
                'label' => 'foto',
                'required' => true,
                'mapped' => false,
                'error_bubbling' => true,
                'constraints' => new File(['maxSize' => '2048k', 'mimeTypes' => ['image/jpeg', 'image/png',], 'mimeTypesMessage' => 'Upload alstublieft ene geldig foto type(png, jpeg , jpg)'
                ])])
            ->add('submit', SubmitType::class, [
                'label' => 'versturen',
                'attr' => [
                    'class' => 'btn-light cst-btn bg-color text-white  pl-3 pr-3 p-2 mt-3 mb-5'
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
