<?php

namespace App\Form;

use App\Entity\ForumText;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ForumTopicFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextareaType::class, [
                'label' => 'tekstvak',
                'required' => true,
                'error_bubbling' => true,
                'attr' => array(
                    'class' => 'w-100',
                )])
            ->add('img', FileType::class, [
                'label' => 'Foto Toevoegen',
                'mapped' => false,
                'required'=> false,
                'error_bubbling' => true,
                'constraints' => new File(['maxSize' => '2048k', 'mimeTypes' => ['image/jpeg', 'image/png',], 'mimeTypesMessage' => 'Upload alstublieft ene geldig foto type(png, jpeg , jpg)'
                ])])
            ->add('submit', SubmitType::class, [
                'label' => 'versturen',
                'attr' => [
                    'class' => 'btn-light cst-btn bg-color text-white  pl-3 pr-3 p-2 mt-3 mb-5'
                ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ForumText::class,
        ]);
    }
}
