<?php

namespace App\Form;

use App\Entity\Document;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class DocumentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        {
            $builder
                ->add('title', TextType::class, [
                    'label' => 'Titel:',
                    'required' => true,
                    'error_bubbling' => true,
                    'attr' => [
                        'class' => 'w-100 mh-25 flush',
                        'placeholder' => 'titel'
                    ]])
                ->add('description', TextareaType::class, [
                    'label' => 'Beschrijving: ',
                    'required' => true,
                    'error_bubbling' => true,
                    'attr' => [
                        'class' => 'w-100 mh-25 flush',
                        'placeholder' => 'beschrijving',
                        ]])
                ->add('document', FileType::class, [
                    'label' => 'Document toevoegen: ',
                    'attr' => ['class'=>'mb-1 mt-1'],
                    'label_attr' => ['class'=> 'mtpl-fls'],
                    'required' => true,
                    'mapped' => false,
                    'error_bubbling' => true,
                    'constraints' => new File(['maxSize' => '2048k', 'mimeTypes' => ['application/pdf',
                        'application/x-pdf', 'image/jpeg',
                        'image/png' ], 'mimeTypesMessage' => 'Upload alstublieft ene geldig Document (pdf, png, jpeg, jpg)'])])
                ->add('img', FileType::class, [
                    'label' => 'Foto van kaft toevoegen: ',
                    'required' => false,
                    'mapped' => 'false',
                    'constraints' => [
                        new File([
                            'maxSize' => '2048k',
                            'mimeTypes' => [
                                'image/jpeg',
                                'image/png'
                            ]
                            ,
                            'mimeTypesMessage' => 'Upload alstublieft ene geldig foto type(png, jpeg , jpg)',

                        ])
                    ]
                ])
                ->add('category', ChoiceType::class, [
                    'error_bubbling'=>'true',
                    'choices'=>[
                        'Artikelen uit tijdschriften' => 1,
                        'Divers' => 2,
                        'Folders' => 3,
                        'Handleidingen'=> 4,
                        'Koeriers'=> 5,
                        'Motorisch'=>6,
                        'Prijslijsten' => 7,
                        'Club merchandise' => 8,
                        'Club documenten' => 9,
                        'Ontwerp tekeningen' => 10,
                        'Bijdragen van leden' => 11,
                    ],
                    'label'=>'Categorie: ',
                    'required'=>true
                ])
                ->add('forAll', CheckboxType::class, [
                    'label'=> 'voor iedereen beschikbaar: ',
                    'required'=>false
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'versturen', 'attr' => [
                        'class' => 'btn-light cst-btn bg-color text-white mt-3 form-control cst-sub mb-5']
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Document::class,
        ]);
    }
}
