<?php

namespace App\Form;

use App\Entity\PersonalDetail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonalDetailFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Voornaam',
                'error_bubbling' => true,
                'required'=>false
            ])
            ->add('middlename', TextType::class, [
                'label' => 'Tussenvoegsel',
                'error_bubbling' => true,
                'required'=>false
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Achternaam',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'w-100'
                ],
                'required'=>false
            ])
            ->add('birthday', DateType::class, [
                'label' => 'Geboordtedatum',
                'error_bubbling' => true,
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format'=>'dd-MM-yyyy',
                'html5' => false,
                'required'=>false
            ])
            ->add('gender', ChoiceType::class, [
                'label'=>'Geslacht',
                'error_bubbling' => true,
                'attr' => ['class' => 'p-1 w-100'],
                'choices' => [
                    'Niet gespecificeerd' => 'Niet gespecificeerd',
                    'man' => 'man',
                    'vrouw' => 'vrouw'
                ],

            ])
            ->add('streetname', TextType::class,[
                'label'=>'Straatnaam',
                'error_bubbling' => true,
                'required'=>false
            ])
            ->add('housenumber', TextType::class,[
                'label'=>'Huisnummer',
                'error_bubbling' => true,
                'required'=>false
            ])
            ->add('ZIPcode', TextType::class,[
                'label'=>'Postcode',
                'error_bubbling' => true,
                'required'=>false
            ])
            ->add('city', TextType::class,[
                'label'=>'Stad',
                'error_bubbling' => true,
                'required'=>false
            ])
            ->add('province', TextType::class,[
                'label'=>'Provincie',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'w-100'
                ],
                'required'=>false

            ])
            ->add('country', TextType::class,[
                'label'=>'Land',
                'error_bubbling' => true,
                'attr' => [
                    'class' => 'w-100'
                ],
                'required'=>false
            ])
            ->add('phonenumber', TelType::class,
                [
                    'label'=>'Telefoonnummer',
                    'error_bubbling' => true,
                    'attr' => [
                        'class' => 'w-100'
                    ],
                    'required'=>false
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PersonalDetail::class,
        ]);
    }
}
