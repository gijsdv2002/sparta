<?php

namespace App\Form;

use App\Entity\ForumCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ForumCategoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'=>'titel',
                'required' => 'true',
                'error_bubbling' => 'true',
                'attr' => [
                    'class' => 'w-100',
                    'placeholder'=>'titel'
                ]

            ])
            ->add('submit', SubmitType::class,[
                'label'=>'toevoegen',
                'attr' => [
                    'class' => 'btn-light cst-btn bg-color text-white mt-3 form-control cst-sub mb-5']
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ForumCategory::class,
        ]);
    }
}
