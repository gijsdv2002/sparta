<?php


namespace App\Controller;


use App\Entity\Contact;
use App\Entity\News;
use App\Form\ContactFormType;
use App\Repository\ContactRepository;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET","POST"})
     * @param NewsRepository $newsRepository
     * @return Response
     */
//    load new(index)page and add news block
    public function news(NewsRepository $newsRepository): Response
    {

        if (isset($_POST['publiceer'], $_POST['title'], $_POST['text'])) {

            $newsManager = $this->getDoctrine()->getManager();

            $news = new News();

            $news->setTitle($_POST['title']);
            $news->setText($_POST['text']);
            $newsManager->persist($news);
            $newsManager->flush();
            return $this->redirectToRoute('index');
        }


        return $this->render('index.html.twig', [
            'articles' => $newsRepository->findAll(),]);

    }

    /**
     * @Route("/overons", name="about", methods={"GET"})
     */
//  load about page
    public function About(): Response
    {

        return $this->render('about.html.twig');
    }

    /**
     * @Route("/contact", name="contact", methods={"GET","POST"})
     * @param ContactRepository $contactRepository
     * @param SluggerInterface $slugger
     * @param Request $request
     * @return Response
     */
//  load contact page and add contact
    public function contact(ContactRepository $contactRepository, SluggerInterface $slugger, Request $request): Response
    {
        $contact = new Contact();
//  genarate form
        $form = $this->createForm(ContactFormType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $contactfile
             */
//    handle img of contact
            $contactfile = $form->get('img')->getData();
            $originalFilename = pathinfo($contactfile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename . '-' . uniqid('', true) . '.' . $contactfile->guessExtension();

            try {
                $newFilename = $contactfile->move($this->getParameter('contact_directory'), $newFilename);
            } catch (FileException $e) {
            }
            $contact->setImg($newFilename);

            $contactManager = $this->getDoctrine()->getManager();
            $contactManager->persist($contact);
            $contactManager->flush();
            return $this->redirectToRoute('contact');

        }
//      render page and form
        return $this->render('contact/contact.html.twig', ['form' => $form->createView(), 'contacts' => $contactRepository->findAll()]);
    }

    /**
     * @Route("/contact/{id}", name="contact_delete", methods={"DELETE"})
     */
//    delete contact
    public function contactDelete(Request $request, Contact $contact): Response
    {
        if ($this->isCsrfTokenValid('delete' . $contact->getId(), $request->request->get('_token'))) {
            $filesystem = new Filesystem();
            $filesystem->remove($contact->getImg());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contact);
            $entityManager->flush();
        }
        return $this->redirectToRoute('contact');

    }


}