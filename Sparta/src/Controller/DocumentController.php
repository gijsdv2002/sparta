<?php


namespace App\Controller;


use App\Entity\Document;
use App\Form\DocumentFormType;
use App\Repository\DocumentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;


/**
 * @Route("/document")
 */
class DocumentController extends AbstractController
{
    /**
     * @Route("/", name="document", methods={"GET","POST"})
     * @param DocumentRepository $documentRepository
     * @return Response
     */
//load all document file
    public function document(DocumentRepository $documentRepository): Response
    {
        return $this->render('document/document.html.twig', ['documents' => $documentRepository->findAll()]);
    }

    /**
     * @Route("-category/{number}", name="document_category", methods={"GET","POST"})
     * @param $number
     * @param DocumentRepository $documentRepository
     * @return Response
     */
//load all files with category
    public function CategoryDocument($number, DocumentRepository $documentRepository): Response
    {
        return $this->render('document/document.html.twig', ['documents' => $documentRepository->findBy(['Category'=>$number])]);
    }

    /**
     * @Route("/new", name="document_new", methods={"GET","POST"})
     * @param SluggerInterface $slugger
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
//  add new document
    public  function newDocument(SluggerInterface $slugger, Request $request){

        $document = new Document();

        $form = $this->createForm(DocumentFormType::class, $document);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            upload document
            /** @var UploadedFile $documentfile
             */
            $documentfile = $form->get('document')->getData();
            $originalDocumentFilename = pathinfo($documentfile->getClientOriginalName(), PATHINFO_FILENAME);
            $safeDocumentFilename = $slugger->slug($originalDocumentFilename);
            $newDocumentFilename = $safeDocumentFilename . '-' . uniqid('', true) . '.' . $documentfile->guessExtension();

            try {
                $newDocumentFilename = $documentfile->move($this->getParameter('document_directory'), $newDocumentFilename);
            } catch (FileException $e) {
            }

//          upload foto kaft
            /** @var UploadedFile $imgfile
             */
            $imgfile = $form->get('img')->getData();
            if ($imgfile) {
                $originalImgFilename = pathinfo($imgfile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeImgFilename = $slugger->slug($originalImgFilename);
                $newImgFilename = $safeImgFilename . '-' . uniqid('', true) . '.' . $imgfile->guessExtension();

                try {
                    $newImgFilename = $imgfile->move($this->getParameter('document_img_directory'), $newImgFilename);
                } catch (FileException $e) {
                }
                $document->setImg($newImgFilename);
            }



            $document->setDocument($newDocumentFilename);

            $documentManager = $this->getDoctrine()->getManager();
            $documentManager->persist($document);
            $documentManager->flush();
            return $this->redirectToRoute('document');

        }
        return $this->render('document/new.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("/{id}", name="document_delete", methods={"DELETE"})
     * @param Request $request
     * @param Document $document
     * @return Response
     */
//    delete document
    public function documentDelete(Request $request, Document $document): Response
    {
        if ($this->isCsrfTokenValid('delete' . $document->getId(), $request->request->get('_token'))) {
            $filesystem = new Filesystem();
            if ($document->getImg() !== null){
                $filesystem->remove($document->getImg());
            }
            $filesystem->remove($document->getDocument());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($document);
            $entityManager->flush();
        }
        return $this->redirectToRoute('document');


    }
}