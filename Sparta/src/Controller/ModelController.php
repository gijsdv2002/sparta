<?php


namespace App\Controller;


use App\Entity\Model;
use App\Form\ModelFormType;
use App\Repository\ModelInfoRepository;
use App\Repository\ModelRepository;
use App\Repository\SliderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route ("/model")
 */
class ModelController extends AbstractController
{
    /**
     * @param ModelRepository $modelRepository
     * @Route ("/", name="model_index")
     * @return Response
     */
//    load all models
    public function index(ModelRepository $modelRepository): Response
    {
        return $this->render('model/modelList.html.twig', ['models' => $modelRepository->findAll()]);
    }

    /**
     * @Route("/modelinfo/{id}", name="model_info", methods={"GET","POST"})
     * @param $id
     * @param ModelRepository $modelRepository
     * @param SliderRepository $sliderRepository
     * @param ModelInfoRepository $modelInfoRepository
     * @return Response
     */
//        load individual model
    public function ModelInfo($id, ModelRepository $modelRepository, SliderRepository $sliderRepository, ModelInfoRepository $modelInfoRepository): Response
    {
        return $this->render('model/modelInfo.html.twig', [
            'model' => $modelRepository->findOneBy(['id' => $id]),
            'modelsInfo' => $modelInfoRepository->findInfo($id),
            'sliders' => $sliderRepository->findBy(['modelID' => $id])]);
    }


    /**
     * @Route("/new", name="model_new", methods={"GET","POST"})
     * @param SluggerInterface $slugger
     * @param Request $request
     * @return RedirectResponse|Response
     */

    public function newModel(SluggerInterface $slugger, Request $request)
    {

        $model = new Model();

        $form = $this->createForm(ModelFormType::class, $model);
        $form->handleRequest($request);

//          check if form is submit
        if ($form->isSubmitted() && $form->isValid()) {
//         logic to handle img of model
            /** @var UploadedFile $imgfile
             */
            $imgfile = $form->get('img')->getData();
            if ($imgfile) {
                $originalImgFilename = pathinfo($imgfile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeImgFilename = $slugger->slug($originalImgFilename);
                $newImgFilename = $safeImgFilename . '-' . uniqid('', true) . '.' . $imgfile->guessExtension();

                try {
                    $newImgFilename = $imgfile->move($this->getParameter('model_img_directory'), $newImgFilename);
                } catch (FileException $e) {
                }
                $model->setImg($newImgFilename);
            }

//         logic to handle img of slider these one is duplicate because otherwise the handling of the slider was too complicated
            $sliderfiles = $form->get('sliderID')->getData();
            foreach ($sliderfiles as $file) {
                /** @var UploadedFile $sliderfile */
                $sliderfile = $file->getFile();
                $originalSliderFilename = pathinfo($sliderfile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeSliderFilename = $slugger->slug($originalSliderFilename);
                $newSliderFilename = $safeSliderFilename . '-' . uniqid('', true) . '.' . $sliderfile->guessExtension();
                try {
                    $newSliderFilename = $sliderfile->move($this->getParameter('model_img_directory'), $newSliderFilename);
                } catch (FileException $e) {
                }
                $file->setSliderImg($newSliderFilename);
            }
//            submit form
            $modelManager = $this->getDoctrine()->getManager();
            $modelManager->persist($model);
            $modelManager->flush();
            return $this->redirectToRoute('model_index');

        }
//        render page
        return $this->render('model/new.html.twig', ['form' => $form->createView()]);

    }
}