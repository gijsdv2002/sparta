<?php


namespace App\Controller;


use App\Entity\ForumCategory;
use App\Entity\ForumSubject;
use App\Entity\ForumText;
use App\Form\ForumCategoryFormType;
use App\Form\ForumSubjectFormType;
use App\Form\ForumTopicFormType;
use App\Repository\ForumCategoryRepository;
use App\Repository\ForumSubjectRepository;
use App\Repository\ForumTextRepository;
use DateTime;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class ForumController
 * @package App\Controller
 * @Route("/forum")
 */
class ForumController extends AbstractController
{

    /**
     * @Route("/", name="forum_index", methods={"GET"})
     * @param ForumSubjectRepository $forumSubjectRepository
     * @param ForumCategoryRepository $forumCategoryRepository
     * @return Response
     */
//    load subject page of forum
    public function index(ForumSubjectRepository $forumSubjectRepository, ForumCategoryRepository $forumCategoryRepository): Response
    {

        return $this->render('forum/forum.html.twig', ['subjects' => $forumSubjectRepository->findAllGroupedByCategory(), 'counts'=> $forumCategoryRepository->findAll() ]);

    }

    /**
     * @Route("/{subject}", name="forum_category", methods={"GET"})
     * @param $subject
     * @param ForumCategoryRepository $categoryRepository
     * @param ForumSubjectRepository $forumSubjectRepository
     * @return Response
     */
//    load list with  category of forum
    public function forumCategory($subject, ForumCategoryRepository $categoryRepository, ForumSubjectRepository $forumSubjectRepository): Response
    {
        $subjectInfo = $forumSubjectRepository->findOneBy(['id'=>$subject]);
        if (!$subjectInfo){
            throw $this->createNotFoundException(
                'Onderwerp niet gevonden'
            );
        }

        return $this->render('forum/forumCategory.html.twig', [
            'categories' => $categoryRepository->findBy(['subjectID' => $subject]),
            'subject' => $subject,
            'subjectInfo' => $subjectInfo
        ]);

    }


    /**
     * @Route("/{subject}/{category}", name="forum_topic", methods={"GET","POST"})
     * @param $subject
     * @param $category
     * @param ForumTextRepository $forumTextRepository
     * @param ForumCategoryRepository $forumCategoryRepository
     * @param Request $request
     * @param SluggerInterface $slugger
     * @return Response
     */
//    load page with the reactions of the topic and add them
    public function forumTopic($subject, $category, ForumTextRepository $forumTextRepository, ForumCategoryRepository $forumCategoryRepository, Request $request, SluggerInterface $slugger): Response
    {
        $categoryInfo = $forumCategoryRepository->findOneBy(['id' => $category]);
        if (!$categoryInfo) {
            throw $this->createNotFoundException(
                'Topic niet gevonden'
            );
        }
        $forumText = new ForumText();

        $form = $this->createForm(ForumTopicFormType::class, $forumText);

        $form->handleRequest($request);

        // make add new text to topic
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $forumTextfile
             */

            //handle img
            $forumTextfile = $form->get('img')->getData();
            if ($forumTextfile) {
                $originalFilename = pathinfo($forumTextfile->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid('', true) . '.' . $forumTextfile->guessExtension();

                try {
                    $newFilename = $forumTextfile->move($this->getParameter('contact_directory'), $newFilename);
                } catch (FileException $e) {
                }
                $forumText->setImg($newFilename);

            }

            $categoryEntity = $this->getDoctrine()->getRepository(ForumCategory::class)->find($category);
            $newReaction = $categoryEntity->getReactions() +1;
            $categoryEntity->setReactions($newReaction);
            $forumText->setUserID($this->getUser());
            $forumText->setPublished(new DateTime());
            $forumText->setCategoryID($categoryEntity);
            $forumText->setSubject($subject);
            $topicManager = $this->getDoctrine()->getManager();
            $topicManager->persist($forumText);
            $topicManager->flush();
            return $this->redirectToRoute('forum_topic', ['subject' => $subject, 'category' => $category]);

        }

        return $this->render('forum/forumTopic.html.twig', [
            'form' => $form->createView(),
            'topics' => $forumTextRepository->findBy(['categoryID' => $category, 'subject' => $subject]),
            'subject' => $subject,
            'category' => $categoryInfo
        ]);

    }

    /**
     * @Route("-new", name="forum_subject_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function subjectNew(Request $request)
    {
        $forumSubject = new ForumSubject();

        $form = $this->createForm(ForumSubjectFormType::class, $forumSubject);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $categoryManager = $this->getDoctrine()->getManager();
            $categoryManager->persist($forumSubject);
            $categoryManager->flush();
            return $this->redirectToRoute('forum_index');
        }
        return $this->render('forum/subjectNew.html.twig', ['form' => $form->createView()]);

    }

    /**
     * @Route("-new-category/{subject}", name="forum_category_new", methods={"GET","POST"})
     * @param $subject
     * @param Request $request
     * @param ForumSubjectRepository $forumSubjectRepository
     * @return Response
     */
    public function categoryNew($subject, Request $request, ForumSubjectRepository $forumSubjectRepository)
    {
        $forumCategory = new ForumCategory();

        $form = $this->createForm(ForumCategoryFormType::class, $forumCategory);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $forumCategory->setUserID($this->getUser());
            $forumCategory->setSubjectID($forumSubjectRepository->findOneBy(['id' => $subject]));
            $forumCategory->setReactions(0);
            $categoryManager = $this->getDoctrine()->getManager();
            $categoryManager->persist($forumCategory);
            $categoryManager->flush();
            return $this->redirectToRoute('forum_category', ['subject' => $subject]);

        }
        return $this->render('forum/categoryNew.html.twig', ['form' => $form->createView(), 'subject' => $subject]);

    }
}